﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Util
{
    public static class AlphanumericGenerator
    {
        public static string generate(int lenght)
        {
            Random rand = new Random();

            var alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var gen = new string(Enumerable.Repeat(alphanum, lenght).Select(x => x[rand.Next(x.Length)]).ToArray());

            return gen;
        }
    }
}

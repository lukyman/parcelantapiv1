﻿using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.BusinessLogic.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic
{
    public interface IUnitOfWork : IDisposable
    {

        IClientBranchRepository ClientBranch { get; }
        IClientRepository Clients { get; }
        IAppUserRepository AppUser { get; }
        IAppUserRoleRepository AppUserRole { get; }
        IClientAppUserRepository ClientAppUser { get; }
        IAppRoleRepository AppRole { get; }
        ICountyRepository County { get; }
        IRouteStopRepository RouteStop { get; }
        IRouteRepository Route { get; }
        IClientBranchRouteStopRepository ClientBranchRouteStop { get; }
        IParcelRepository Parcel { get; }
        IParcelStatusRepository ParcelStatus { get; }
        ICustomerRepository Customer { get; }
        ICollectionPointRepository CollectionPoint { get; }
        IClientParcelNumberCounterRepository ClientParcelNumberCounter { get; }
        IParcelItemRepository ParcelItem { get; }
        IParcelStatusDetailRepository ParcelStatusDetail { get; }
        IParcelItemStatusRepository ParcelItemStatus { get; }
        IParcelItemStatusDetailRepository ParcelItemStatusDetail { get; }
        IParcelCurrentStatusRepository ParcelCurrentStatus { get; }
        IParcelItemCurrentStatusRepository ParcelItemCurrentStatus { get; }
        IClientDispatchCounterRepository ClientDispatchCounter { get; }
        IDispatchGroupRepository DispatchGroup { get; }
        IDispatchedParcelsRepository DispatchedParcels { get; }
        ICarrierRepository Carrier { get; }
        int Complete();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain
{
    public static class ZeroPrefix
    {
       public static string Handreds(long number)
        {
            if (number < 100)
            {
                return String.Format("{0:000}", number);
            }
            else
            {
                return number.ToString();
            }
        }
    }
}

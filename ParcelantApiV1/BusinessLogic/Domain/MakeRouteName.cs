﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain
{
    public static class MakeRouteName
    {
        public static string OriginToDestination(string origin,string destination)
        {
            return origin + " to " + destination;
        }
    }
}

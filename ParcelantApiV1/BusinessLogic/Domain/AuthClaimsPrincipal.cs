﻿using Microsoft.AspNetCore.Http;
using ParcelantApiV1.Businesslogic.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Domain
{
    public class AuthClaimsPrincipal : ClaimsPrincipal, IAuthClaimsPrincipal
    {
        private IEnumerable<Claim> _userclaims;
        private readonly IHttpContextAccessor _httpcontext; 
        public AuthClaimsPrincipal(IHttpContextAccessor httpContext)
        {
            _httpcontext = httpContext;
            _userclaims = _httpcontext.HttpContext.User.Claims;
            
            
        }
        private string getClaim(string claimType)
        {
           return _userclaims.Where(x => x.Type == claimType).FirstOrDefault().Value;
        }

        public int getClientId()
        {
            return Convert.ToInt32(getClaim("ClientId"));
        }
    }
}

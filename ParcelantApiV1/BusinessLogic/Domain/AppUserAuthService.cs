﻿using AutoMapper;
using ParcelantApiV1.Businesslogic.Domain.Interfaces;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.ViewModel.AppUser;
using System.Linq;

namespace ParcelantApiV1.Businesslogic.Domain
{
    public class AppUserAuthService : IAppUserAuthService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public AppUserAuthService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public AppUserInfoViewModel GetAppUserInfo(string username)
        {
            var appUser = _unitOfWork.AppUser.GetAppUserByUsername(username);
            var clientAppUser = _unitOfWork.ClientAppUser.GetAppUserInfo(appUser.Id);

            return _mapper.Map<ClientAppUser, AppUserInfoViewModel>(clientAppUser);
           
        }

        public bool IsValidAccount(string username, string plainPassword)
        {
            var hashedpassword = Util.PasswordCrypto.HashPassword(plainPassword);
            var actualPassword = _unitOfWork.AppUser.Find(x => x.Username.ToLower() == username.ToLower()).FirstOrDefault().Password;
            if (Util.PasswordCrypto.VerifyHashedPassword(actualPassword,plainPassword))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        
    }
}

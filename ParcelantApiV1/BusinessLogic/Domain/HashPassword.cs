﻿using ParcelantApiV1.Businesslogic.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Domain
{
    public class HashPassword : IHashPassword
    {
        public string hash(string plainText)
        {
            return Util.PasswordCrypto.HashPassword(plainText);
        }
    }
}

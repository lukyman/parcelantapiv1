﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain
{
    public static class FormatParcelNumber
    {
        public static string Format(long ParcelNumber)
        {
            if (ParcelNumber < 100)
            {
                return String.Format("{0:000}", ParcelNumber);
            }
            else
            {
                return ParcelNumber.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Domain.Interfaces
{
    public interface IHashPassword
    {
        string hash(string plainText);
    }
}

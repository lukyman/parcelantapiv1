﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain.Interfaces
{
    public interface IParcelStatusManager
    {
        void UpdateParcelStatus(Client client, ClientBranch clientBranch , AppUser appUser, UpdateParcelStatusViewModel updateParcelStatusViewModel,
             ParcelStatus intendedParcelStatus, ParcelStatus fallbackParcelStatus,
             ParcelItemStatus intendedParcelItemStatus, ParcelItemStatus fromParcelItemStatus, bool hasNext);
       
    }
}

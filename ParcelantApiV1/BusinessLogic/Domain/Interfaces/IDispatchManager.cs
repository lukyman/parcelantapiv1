﻿using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain.Interfaces
{
    public interface IDispatchManager
    {
        void ProcessClientDispatch(Client client, ClientBranch branch, AppUser appUser, Carrier carrier, IEnumerable<int> parcelIdList);
        
    }
}

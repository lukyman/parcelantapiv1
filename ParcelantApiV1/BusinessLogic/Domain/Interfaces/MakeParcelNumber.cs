﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain.Interfaces
{
    public static class MakeParcelNumber
    {
        public static long Make(long LastParcelCounter)
        {
            return LastParcelCounter + 1;
        }
    }
}

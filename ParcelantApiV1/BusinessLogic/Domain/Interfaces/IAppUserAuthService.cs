﻿using ParcelantApiV1.ViewModel.AppUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Domain.Interfaces
{
    public interface IAppUserAuthService:IAuthService
    {
        AppUserInfoViewModel GetAppUserInfo(string username);
       
    }
}

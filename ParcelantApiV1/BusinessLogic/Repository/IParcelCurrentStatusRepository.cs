﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface IParcelCurrentStatusRepository:IRepository<ParcelCurrentStatus>
    {
       ParcelCurrentStatus GetCurentStatus(int clientId, int parcelId);
        IEnumerable<ParcelCurrentStatus> GetAllParcelCurrentStatus(int clientId, int branchId, int intendedParcelStatus, int fallbackParcelStatus);
        IEnumerable<ParcelCurrentStatus> GetAllParcelsToReceive(int clientId,int branchid, int intendedParcelStatus, int fallbackParcelStatus, int destinationId);
    }
}

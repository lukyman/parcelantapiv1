﻿using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface IDispatchGroupRepository:IRepository<DispatchGroup>
    {
    }
}

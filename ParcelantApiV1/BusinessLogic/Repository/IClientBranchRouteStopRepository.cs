﻿using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface IClientBranchRouteStopRepository : IRepository<ClientBranchRouteStop>
    {
        ClientBranchRouteStop GetBranchDestination(int clientId, int branchId);
        ClientBranchRouteStop GetBranchOrigin(int clientId, int branchId);
      }
}

﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Repository
{
    public interface IClientBranchRepository:IRepository<ClientBranch>
    {
       IEnumerable<ClientBranch> GetAllClientBranch(int clientId);
        ClientBranch GetClientBranch(int clientId, int branchId);
    }
}

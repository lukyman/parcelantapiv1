﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic.Repository
{
    public interface IClientAppUserRepository : IRepository<ClientAppUser>
    {
        IEnumerable<ClientAppUser> getClientAppUsers(int clientId);
        ClientAppUser GetAppUserInfo(int appUserId);
    }
}

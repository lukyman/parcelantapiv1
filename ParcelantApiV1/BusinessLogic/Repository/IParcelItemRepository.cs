﻿using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface IParcelItemRepository:IRepository<ParcelItem>
    {
        ParcelItem GetParcelItem(int parcelid, int parcelitemid);
        IEnumerable<ParcelItem> GetParcelItems( int parcelid);
    }
}

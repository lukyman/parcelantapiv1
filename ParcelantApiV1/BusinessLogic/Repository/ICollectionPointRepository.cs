﻿using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Repository
{
    public interface ICollectionPointRepository:IRepository<CollectionPoint>
    {
        IEnumerable<CollectionPoint> GetClientCollectionPoints(int clientId);
        CollectionPoint GetClientCollectionPoint(int clientId, int collectionPointId);

    }
}

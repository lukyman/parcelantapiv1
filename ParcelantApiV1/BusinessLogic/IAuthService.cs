﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.Businesslogic
{
   public interface IAuthService
    {
      bool  ValidatePassword(string plainText, string hashed);
    }


    public interface IAuthPortalUser: IAuthService
    {
        
    }

    public class AuthPortalUser : IAuthPortalUser
    {
        public bool ValidatePassword(string plainText, string hashed)
        {
           return Util.PasswordCrypto.VerifyHashedPassword(hashed, plainText);
        }

    }
}

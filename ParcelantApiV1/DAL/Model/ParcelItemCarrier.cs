﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Models
{
    public class ParcelItemCarrier
    {
        public int Id { get; set; }
        public Carrier Carrier { get; set; }
        public Client Client { get; set; }
        public Parcel Parcel { get; set; }
        public ParcelItem ParcelItem { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

    }
}

﻿using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Model
{
    public class ClientParcelNumberCounter
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public long ParcelNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get;  set; }
    }
}

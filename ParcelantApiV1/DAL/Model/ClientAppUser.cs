﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Models
{
    [Table("ClientAppUser")]
    public class ClientAppUser
    {
        public int Id { get; set; }
        public AppUser AppUser { get; set; }
        public Client Client { get; set; }
        public ClientBranch ClientBranch { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}

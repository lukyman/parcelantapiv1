﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Models
{
    public class ParcelItem
    {
        public int Id { get; set; }
        public Parcel Parcel { get; set; }
        public AppUser AppUser { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Models
{
    public class ParcelCurrentStatus
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public AppUser AppUser { get; set; }
        public ParcelStatus ParcelStatus { get; set; }
        public Parcel Parcel { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}

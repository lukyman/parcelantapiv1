﻿using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Model
{
    public class DispatchGroup
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public Carrier Carrier { get; set; }
        public AppUser AppUser { get; set; }
        public bool IsClosed { get; set; }
        public string GroupNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}

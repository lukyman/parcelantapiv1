﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ParcelItemRepository : Repository<ParcelItem>, IParcelItemRepository
    {
        public ParcelItemRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ParcelItem GetParcelItem(int parcelid, int parcelitemid)
        {
           return Context.ParcelItem
                    .Include(x => x.AppUser)
                    .Include(x => x.Parcel)
                    .Where(y => y.Parcel.Id == parcelid && y.Id == parcelitemid)
                    .FirstOrDefault();
        }

        public IEnumerable<ParcelItem> GetParcelItems(int parcelid)
        {
            return Context.ParcelItem
                       .Include(x => x.AppUser)
                       .Include(x => x.Parcel)
                       .Where(y => y.Parcel.Id == parcelid )
                       .ToList();
        }
    }
}

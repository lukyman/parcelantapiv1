﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;

namespace ParcelantApiV1.DAL.Repositoy
{
    public class RouteRepository : Repository<Routes>, IRouteRepository
    {
        public RouteRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public IEnumerable<RouteStop> GetClientBranchRoutes(int clientId, int branchId)
        {
            /*var query = (from routes in Context.Routes
                         join clientBranchStops in Context.ClientBranchRouteStop on routes.Client.Id equals clientBranchStops.ClientBranch.Client.Id
                         where clientBranchStops.ClientBranch.Id != branchId
                         select new Routes
                         {
                             Id = routes.Id,
                             Client = routes.Client,
                             Destination = routes.Destination,
                             Origin = routes.Origin,
                             CreatedAt = routes.CreatedAt,
                             UpdatedAt = routes.UpdatedAt
                         }).ToList();*/

            var query = (from clientBranchStops in Context.ClientBranchRouteStop
                         join destination in Context.RouteStop on clientBranchStops.RouteStop.Id equals destination.Id
                         join route in Context.Routes on destination.Id equals route.Destination.Id
                         where clientBranchStops.ClientBranch.Id != branchId && destination.Client.Id == clientId
                         select new RouteStop
                         {
                             Id = destination.Id,
                             Client = destination.Client,
                             Name = destination.Name,
                             CreatedAt = destination.CreatedAt,
                             UpdatedAt = destination.UpdatedAt
                         }
                         ).ToList();
            return query;

        
        }

        public Routes GetClientRoute(int clientId, int routeId)
        {
            return Context.Routes
                    .Include(x => x.Destination)
                    .Include(x => x.Origin)
                    .Where(x => x.Client.Id == clientId && x.Id==routeId).FirstOrDefault();

        }

        public IEnumerable<Routes> GetClientRoutes(int clientId)
        {
            return Context.Routes
                    .Include(x => x.Destination)
                    .Include(x => x.Origin)
                    .Where(x => x.Client.Id == clientId).ToList();
                
        }

        public Routes GetRouteByOrginAndDestination(int clientid, int originid, int destinationid)
        {
            return Context.Routes
                    .Include(x => x.Destination)
                    .Include(x => x.Origin)
                    .Where(x => x.Destination.Id == destinationid && x.Origin.Id == originid && x.Client.Id == clientid)
                    .FirstOrDefault();
        }
    }
}

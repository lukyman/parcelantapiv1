﻿using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.ViewModel.AppUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repositoy
{
    public class AppUserRepository : Repository<AppUser>, IAppUserRepository
    {
        public AppUserRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public AppUser GetAppUserByUsername(string username)
        {
           return Context.AppUser.Where(x => x.Username.ToLower() == username.ToLower()).FirstOrDefault();
        }
    }
}

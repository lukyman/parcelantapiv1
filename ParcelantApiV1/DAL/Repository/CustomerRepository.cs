﻿using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public Customer GetCustomerByPhone(string phone)
        {
           return Context.Customer.Where(x => x.Phone == phone).FirstOrDefault();
        }
    }
}

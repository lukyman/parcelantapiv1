﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ClientBranchRouteStopRepository : Repository<ClientBranchRouteStop>, IClientBranchRouteStopRepository
    {
        public ClientBranchRouteStopRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ClientBranchRouteStop GetBranchDestination(int clientId, int branchId)
        {
            return Context.ClientBranchRouteStop
                .Include(x => x.ClientBranch)
                .Include(x => x.RouteStop)
                .Where(x => x.ClientBranch.Client.Id == clientId).FirstOrDefault();

        }

        public ClientBranchRouteStop GetBranchOrigin(int clientId, int branchId)
        {

            return Context.ClientBranchRouteStop
                .Include(x => x.ClientBranch)
                .Include(x => x.RouteStop)
                .Where(x => x.ClientBranch.Client.Id == clientId && x.ClientBranch.Id == branchId).FirstOrDefault();

        }
    }
}

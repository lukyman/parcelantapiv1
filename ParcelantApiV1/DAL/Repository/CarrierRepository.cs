﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class CarrierRepository : Repository<Carrier>, ICarrierRepository
    {
        public CarrierRepository(ParcelantApiV1Context context) : base(context)
        {
        }

    public IEnumerable<Carrier> GetClientCarriers(int clientid)
    {
      return Context.Carrier.Include(x => x.Client).Where(x => x.Client.Id == clientid).ToList();
    }

  }
}

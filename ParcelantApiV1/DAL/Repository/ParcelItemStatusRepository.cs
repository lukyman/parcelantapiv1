﻿using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ParcelItemStatusRepository : Repository<ParcelItemStatus>, IParcelItemStatusRepository
    {
        public ParcelItemStatusRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ParcelItemStatus GetParcelItemStatusByName(string statusName)
        {
            return Context.ParcelItemStatus.Where(x => x.Name == statusName.ToUpper()).FirstOrDefault();
        }
    }
}

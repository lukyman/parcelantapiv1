﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ParcelRepository : Repository<Parcel>, IParcelRepository
    {
        public ParcelRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public Parcel GetParcelByClientBranch(int clientId, int branchId, int parcelId)
        {
            return Context.Parcel
                .Include(x => x.AppUser)
                .Include(x => x.Sender)
                .Include(x => x.Receiver)
                .Include(x => x.Routes)
                .Include(x => x.CollectionPoint)
                .Include(x => x.Client)
                .Include(x => x.ClientBranch)
                .Where(x => x.Client.Id == clientId && x.ClientBranch.Id == branchId && x.Id == parcelId).FirstOrDefault();
        }

        public Parcel GetParcelByParcelNumber(int clientId,string parcelNumber)
        {
            return Context.Parcel
                .Include(x=>x.AppUser)
                .Include(x => x.Sender)
                .Include(x => x.Receiver)
                .Include(x => x.Routes)
                .Include(x => x.CollectionPoint)
                .Include(x => x.Client)
                .Include(x=>x.ClientBranch)
                .Where(x => x.Client.Id == clientId && x.ParcelNumber == parcelNumber).FirstOrDefault();
        }

        public Parcel GetParcelDetail(int clientId,int parcelId)
        {
            return Context.Parcel
                .Include(x=>x.AppUser)
                .Include(x => x.Sender)
                .Include(x => x.Receiver)
                .Include(x => x.Routes).ThenInclude(x=>x.Origin)
                .Include(x => x.Routes).ThenInclude(x => x.Destination)
                .Include(x => x.CollectionPoint)
                .Include(x => x.Client)
                .Include(x=>x.ClientBranch)
                .Where(x => x.Client.Id == clientId && x.Id == parcelId).FirstOrDefault();
        }

        public IEnumerable<Parcel> GetParcels(int clientId)
        {
            return Context.Parcel
                .Include(x=>x.AppUser)
                .Include(x => x.Sender)
                .Include(x => x.Receiver)
                .Include(x => x.Routes)
                .Include(x => x.CollectionPoint)
                .Include(x => x.Client)
                .Include(x=>x.ClientBranch)
                .Include(x => x.Routes).ThenInclude(x => x.Origin)
                .Include(x => x.Routes).ThenInclude(x => x.Destination)
                .Where(x => x.Client.Id == clientId).ToList();

        }
    }
}

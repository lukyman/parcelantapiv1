﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repositoy
{
    public class ClientBranchRepository : Repository<ClientBranch>, IClientBranchRepository
    {
        public ClientBranchRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public IEnumerable<ClientBranch> GetAllClientBranch(int clientId)
        {
           return Context.ClientBranch
                 .Include(x => x.County)
                 .Include(x => x.Client)
                 .Where(x => x.Client.Id == clientId).ToList();
        }

        public ClientBranch GetClientBranch(int clientId, int branchId)
        {
            return Context.ClientBranch.Where(x => x.Client.Id == clientId && x.Id == branchId).FirstOrDefault();
        }
    }
}

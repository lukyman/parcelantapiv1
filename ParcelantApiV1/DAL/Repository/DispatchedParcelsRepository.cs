﻿using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class DispatchedParcelsRepository : Repository<DispatchedParcels>, IDispatchedParcelsRepository
    {
        public DispatchedParcelsRepository(ParcelantApiV1Context context) : base(context)
        {
        }
    }
}

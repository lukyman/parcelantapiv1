﻿using ParcelantApiV1.Businesslogic.Repository;
using ParcelantApiV1.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repositoy
{
    public class CountyRepository : Repository<County>, ICountyRepository
    {
        public CountyRepository(ParcelantApiV1Context context) : base(context)
        {
        }
    }
}

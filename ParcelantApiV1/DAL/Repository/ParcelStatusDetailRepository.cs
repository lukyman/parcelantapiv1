﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ParcelStatusDetailRepository : Repository<ParcelStatusDetail>, IParcelStatusDetailRepository
    {
        public ParcelStatusDetailRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public bool CheckParcelHasStatus(int clientId, int parcelId, int parcelStatusId)
        {
           return Context.ParcelStatusDetail.
                    Where(x => x.Client.Id == clientId
                        && x.Parcel.Id == parcelId
                        && x.ParcelStatus.Id == parcelStatusId)
                        .FirstOrDefault() != null;
        }

        public IEnumerable<ParcelStatusDetail> GetAllParcelStatus(int clientId,int branchId, int intendedParcelStatus, int fallbackParcelStatus)
        {
           return Context.ParcelStatusDetail
                  .Include(x => x.AppUser)
                  .Include(x => x.Parcel).ThenInclude(x => x.Sender)
                  .Include(x => x.Parcel).ThenInclude(x => x.Receiver)
                  .Include(x=>x.Parcel.Routes).ThenInclude(x=>x.Origin)
                  .Include(x => x.Parcel.Routes).ThenInclude(x => x.Destination)
                  .Include(x=>x.Parcel.CollectionPoint)
                  .Include(x => x.ParcelStatus)
                  .Include(x => x.Client)
                  .Include(x => x.Parcel.ClientBranch)
                  .Where(x => x.ParcelStatus.Id == intendedParcelStatus
                      || x.ParcelStatus.Id == fallbackParcelStatus
                      && x.Parcel.Client.Id == clientId
                      && x.Parcel.ClientBranch.Id == branchId)
                      .ToList();

                     
                 
        }
        
    }
}

﻿using Microsoft.EntityFrameworkCore;
using ParcelantApiV1.BusinessLogic.Repository;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.DAL.Repositoy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL.Repository
{
    public class ParcelItemCurrentStatusRepository : Repository<ParcelItemCurrentStatus>, IParcelItemCurrentStatusRepository
    {
        public ParcelItemCurrentStatusRepository(ParcelantApiV1Context context) : base(context)
        {
        }

        public ParcelItemCurrentStatus GetCurrentStatus(int parcelId, int parcelItemId)
        {
            return Context.ParcelItemCurrentStatus
                .Include(x => x.Client)
                .Include(x => x.AppUser)
                .Include(x => x.Parcel)
                .Include(x => x.ParcelItem)
                .Include(x => x.ParcelItemStatus)
                .Where(x => x.Parcel.Id == parcelId && x.ParcelItem.Id == parcelItemId)
                .FirstOrDefault();
        }

        public IEnumerable<ParcelItemCurrentStatus> GetParcelItemsCurrentStatus(int clientid, int parcelId, int itemStatusId)
        {
            return Context.ParcelItemCurrentStatus
                .Include(x=>x.Client)
                .Include(x=>x.AppUser)
                .Include(x=>x.Parcel)
                .Include(x=>x.ParcelItem)
                .Include(x=>x.ParcelItemStatus)
                .Where(x => x.Client.Id == clientid
                       && x.Parcel.Id == parcelId
                       && x.ParcelItemStatus.Id == itemStatusId).ToList();
        }
    }
}

﻿using AutoMapper;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.ViewModel.AppUser;
using ParcelantApiV1.ViewModel.ClientBranch;
using ParcelantApiV1.ViewModel.Parcel;
using ParcelantApiV1.ViewModel.ParcelItem;
using ParcelantApiV1.ViewModel.ParcelStatus;
using ParcelantApiV1.ViewModel.Routes;
using ParcelantApiV1.ViewModel.RouteStop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.DAL
{
    public class AutoMappingProfile:Profile
    {
        public AutoMappingProfile()
        {
            CreateMap<ClientAppUser, AppUserViewModel>()
                .ForMember(to=>to.BranchName,from=>from.MapFrom(y=>y.ClientBranch.Name));

            CreateMap<ClientBranch, ClientBranchViewModel>()
                .ForMember(to => to.CountyName, from => from.MapFrom(y => y.County.Name));


            CreateMap<RouteStop, RouteStopViewModel>();
            CreateMap<Routes, RoutesViewModel>()
                .ForMember(to=>to.Origin,from => from.MapFrom(y=>y.Origin.Name))
                .ForMember(to => to.Destination, from => from.MapFrom(y => y.Destination.Name));
          //  CreateMap<ParcelStatus, ParcelStatusViewModel>();
           
            CreateMap<ClientAppUser, AppUserInfoViewModel>()
                 .ForMember(to => to.Id, from => from.MapFrom(x => x.AppUser.Id))
                  .ForMember(to => to.Username, from => from.MapFrom(x => x.AppUser.Username))
                  .ForMember(to => to.Fullname, from => from.MapFrom(x => x.AppUser.Fullname))
                  .ForMember(to => to.Phone, from => from.MapFrom(x => x.AppUser.Phone))
                  .ForMember(to => to.Active, from => from.MapFrom(x => x.AppUser.Active))
                  .ForMember(to => to.CreatedAt, from => from.MapFrom(x => x.AppUser.CreatedAt))
                  .ForMember(to => to.UpdatedAt, from => from.MapFrom(x => x.AppUser.UpdatedAt))
                  .ForMember(to => to.BranchId, from => from.MapFrom(x => x.ClientBranch.Id))
                  .ForMember(to => to.BranchName, from => from.MapFrom(x => x.ClientBranch.Name))
                  .ForMember(to => to.ClientId, from => from.MapFrom(x => x.Client.Id))
                  .ForMember(to => to.ClientName, from => from.MapFrom(x => x.Client.Name));

            CreateMap<Parcel, ParcelViewModel>()
                .ForMember(to=>to.ClientBranchId,from=>from.MapFrom(x=>x.ClientBranch.Id))
                .ForMember(to => to.ClientBranchName, from => from.MapFrom(x => x.ClientBranch.Name))
                .ForMember(to => to.AppUserName,from=>from.MapFrom(x=>x.AppUser.Fullname))
                .ForMember(to => to.ReceiverName, from => from.MapFrom(x => x.Receiver.FullName))
                .ForMember(to => to.SenderName, from => from.MapFrom(x => x.Sender.FullName))
                .ForMember(to => to.ReceiverPhone, from => from.MapFrom(x => x.Receiver.Phone))
                .ForMember(to => to.SenderPhone, from => from.MapFrom(x => x.Sender.Phone))
                .ForMember(to => to.RouteName, from => from.MapFrom(x => MakeRouteName.OriginToDestination(x.Routes.Origin.Name, x.Routes.Destination.Name)))
                .ForMember(to => to.CollectionPointName, from => from.MapFrom(x => x.CollectionPoint.Name))
                .ForMember(to => to.CreatedAt, from => from.MapFrom(x => x.CreatedAt))
                .ForMember(to => to.UpdatedAt, from => from.MapFrom(x => x.UpdatedAt));

            CreateMap<ParcelItem, ParcelItemViewModel>()
                .ForMember(to => to.AppUserId, from => from.MapFrom(x => x.AppUser.Id))
                .ForMember(to => to.AppUserName, from => from.MapFrom(x => x.AppUser.Fullname))
                .ForMember(to => to.ParcelId, from => from.MapFrom(x => x.Parcel.Id));

            CreateMap<ParcelItemStatusDetail, ParcelItemStatusDetailViewModel>()
                .ForMember(to=>to.Id, from=>from.MapFrom(x=>x.ParcelItem.Id))
                .ForMember(to => to.AppUserId, from => from.MapFrom(x => x.AppUser.Id))
                .ForMember(to => to.AppUserName, from => from.MapFrom(x => x.AppUser.Fullname))
                .ForMember(to => to.ParcelId, from => from.MapFrom(x => x.Parcel.Id))
                .ForMember(to => to.ItemStatusId, from => from.MapFrom(x => x.ParcelItemStatus.Id))
                .ForMember(to => to.ItemStatusName, from => from.MapFrom(x => x.ParcelItemStatus.Name))
                .ForMember(to=>to.Quantity, from => from.MapFrom(x=>x.ParcelItem.Quantity))
                .ForMember(to =>to.Amount, from => from.MapFrom(x=>x.ParcelItem.Amount ))
                .ForMember(to => to.Description, from => from.MapFrom(x=>x.ParcelItem.Description));

            CreateMap<ParcelItemCurrentStatus, ParcelItemCurrentStatusViewModel>()
                .ForMember(to => to.Id, from => from.MapFrom(x => x.ParcelItem.Id))
                .ForMember(to => to.AppUserId, from => from.MapFrom(x => x.AppUser.Id))
                .ForMember(to => to.AppUserName, from => from.MapFrom(x => x.AppUser.Fullname))
                .ForMember(to => to.ParcelId, from => from.MapFrom(x => x.Parcel.Id))
                .ForMember(to => to.ItemStatusId, from => from.MapFrom(x => x.ParcelItemStatus.Id))
                .ForMember(to => to.ItemStatusName, from => from.MapFrom(x => x.ParcelItemStatus.Name))
                .ForMember(to => to.Quantity, from => from.MapFrom(x => x.ParcelItem.Quantity))
                .ForMember(to => to.Amount, from => from.MapFrom(x => x.ParcelItem.Amount))
                .ForMember(to => to.Description, from => from.MapFrom(x => x.ParcelItem.Description));

            CreateMap<ParcelStatusDetail,ParcelToDispatchViewModel>()
                .ForMember(to => to.Id, from=> from.MapFrom(x=>x.Parcel.Id))
                .ForMember(to => to.ParcelStatusDetailId,from =>from.MapFrom(x=>x.Id))
                .ForMember(to => to.ParcelStatusDetailName, from => from.MapFrom(x => x.ParcelStatus.Name))
                .ForMember(to => to.ClientBranchId, from => from.MapFrom(x => x.Parcel.ClientBranch.Id))
                .ForMember(to => to.ClientBranchName, from => from.MapFrom(x => x.Parcel.ClientBranch.Name))
                .ForMember(to => to.AppUserName, from => from.MapFrom(x => x.AppUser.Fullname))
                .ForMember(to => to.ReceiverName, from => from.MapFrom(x => x.Parcel.Receiver.FullName))
                .ForMember(to => to.SenderName, from => from.MapFrom(x => x.Parcel.Sender.FullName))
                .ForMember(to => to.ReceiverPhone, from => from.MapFrom(x => x.Parcel.Receiver.Phone))
                .ForMember(to => to.SenderPhone, from => from.MapFrom(x => x.Parcel.Sender.Phone))
                .ForMember(to => to.RoutesId, from => from.MapFrom(x=>x.Parcel.Routes.Id))
                .ForMember(to => to.RouteName, from => from.MapFrom(x => MakeRouteName.OriginToDestination(x.Parcel.Routes.Origin.Name, x.Parcel.Routes.Destination.Name)))
                .ForMember(to => to.CollectionPointId, from => from.MapFrom(x => x.Parcel.CollectionPoint.Id))
                .ForMember(to => to.CollectionPointName, from => from.MapFrom(x => x.Parcel.CollectionPoint.Name))
                .ForMember(to => to.ParcelNumber, from=>from.MapFrom(x=>x.Parcel.ParcelNumber))
                .ForMember(to => to.CreatedAt, from => from.MapFrom(x => x.CreatedAt))
                .ForMember(to => to.UpdatedAt, from => from.MapFrom(x => x.UpdatedAt));

            CreateMap<ParcelCurrentStatus, ParcelToDispatchViewModel>()
               .ForMember(to => to.Id, from => from.MapFrom(x => x.Parcel.Id))
               .ForMember(to => to.ParcelStatusDetailId, from => from.MapFrom(x => x.Id))
               .ForMember(to => to.ParcelStatusDetailName, from => from.MapFrom(x => x.ParcelStatus.Name))
               .ForMember(to => to.ClientBranchId, from => from.MapFrom(x => x.Parcel.ClientBranch.Id))
               .ForMember(to => to.ClientBranchName, from => from.MapFrom(x => x.Parcel.ClientBranch.Name))
               .ForMember(to => to.AppUserName, from => from.MapFrom(x => x.AppUser.Fullname))
               .ForMember(to => to.ReceiverName, from => from.MapFrom(x => x.Parcel.Receiver.FullName))
               .ForMember(to => to.SenderName, from => from.MapFrom(x => x.Parcel.Sender.FullName))
               .ForMember(to => to.ReceiverPhone, from => from.MapFrom(x => x.Parcel.Receiver.Phone))
               .ForMember(to => to.SenderPhone, from => from.MapFrom(x => x.Parcel.Sender.Phone))
               .ForMember(to => to.RoutesId, from => from.MapFrom(x => x.Parcel.Routes.Id))
               .ForMember(to => to.RouteName, from => from.MapFrom(x => MakeRouteName.OriginToDestination(x.Parcel.Routes.Origin.Name, x.Parcel.Routes.Destination.Name)))
               .ForMember(to => to.CollectionPointId, from => from.MapFrom(x => x.Parcel.CollectionPoint.Id))
               .ForMember(to => to.CollectionPointName, from => from.MapFrom(x => x.Parcel.CollectionPoint.Name))
               .ForMember(to => to.ParcelNumber, from => from.MapFrom(x => x.Parcel.ParcelNumber))
               .ForMember(to => to.CreatedAt, from => from.MapFrom(x => x.CreatedAt))
               .ForMember(to => to.UpdatedAt, from => from.MapFrom(x => x.UpdatedAt));

            CreateMap<ParcelCurrentStatus, ParcelToReceiveViewModel>()
               .ForMember(to => to.Id, from => from.MapFrom(x => x.Parcel.Id))
               .ForMember(to => to.ParcelStatusDetailId, from => from.MapFrom(x => x.Id))
               .ForMember(to => to.ParcelStatusDetailName, from => from.MapFrom(x => x.ParcelStatus.Name))
               .ForMember(to => to.ClientBranchId, from => from.MapFrom(x => x.Parcel.ClientBranch.Id))
               .ForMember(to => to.ClientBranchName, from => from.MapFrom(x => x.Parcel.ClientBranch.Name))
               .ForMember(to => to.AppUserName, from => from.MapFrom(x => x.AppUser.Fullname))
               .ForMember(to => to.ReceiverName, from => from.MapFrom(x => x.Parcel.Receiver.FullName))
               .ForMember(to => to.SenderName, from => from.MapFrom(x => x.Parcel.Sender.FullName))
               .ForMember(to => to.ReceiverPhone, from => from.MapFrom(x => x.Parcel.Receiver.Phone))
               .ForMember(to => to.SenderPhone, from => from.MapFrom(x => x.Parcel.Sender.Phone))
               .ForMember(to => to.RoutesId, from => from.MapFrom(x => x.Parcel.Routes.Id))
               .ForMember(to => to.RouteName, from => from.MapFrom(x => MakeRouteName.OriginToDestination(x.Parcel.Routes.Origin.Name, x.Parcel.Routes.Destination.Name)))
               .ForMember(to => to.CollectionPointId, from => from.MapFrom(x => x.Parcel.CollectionPoint.Id))
               .ForMember(to => to.CollectionPointName, from => from.MapFrom(x => x.Parcel.CollectionPoint.Name))
               .ForMember(to => to.ParcelNumber, from => from.MapFrom(x => x.Parcel.ParcelNumber))
               .ForMember(to => to.CreatedAt, from => from.MapFrom(x => x.CreatedAt))
               .ForMember(to => to.UpdatedAt, from => from.MapFrom(x => x.UpdatedAt));
        }
    }
}

﻿using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.BusinessLogic.Domain
{
    public class ParcelStatusManager : IParcelStatusManager
    {
        private readonly IUnitOfWork _unitOfWork;
        public ParcelStatusManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void UpdateParcelStatus(Client client, ClientBranch clientBranch ,AppUser appUser, UpdateParcelStatusViewModel updateParcelStatusViewModel, 
                                            ParcelStatus intendedParcelStatus, ParcelStatus fallbackParcelStatus,
                                            ParcelItemStatus intendedParcelItemStatus ,ParcelItemStatus fromParcelItemStatus, bool hasNext)
        {

            foreach (var parcel in updateParcelStatusViewModel.UpdateParcelStatus)
            {
                var parcelExist = _unitOfWork.Parcel.GetParcelByClientBranch(client.Id,clientBranch.Id,parcel.ParcelId);

               // var allParcelItemsCount = _unitOfWork.ParcelItemStatusDetail.Get
               
                if (parcelExist != null )
                {
                   var parcelCurrentStatus = _unitOfWork.ParcelCurrentStatus.Get(parcel.ParcelId);


                    var totalParcelItems = _unitOfWork.ParcelItem.GetParcelItems(parcelExist.Id).Count();
                   
                    int totalitemsUpdated = 0;

                    if (totalParcelItems >= parcel.ParcelItems.Count())
                    {

                        foreach (var item in parcel.ParcelItems)
                        {
                            var parcelItemExist = _unitOfWork.ParcelItem.GetParcelItem(parcel.ParcelId, item.ParcelItemId);

                            if (parcelItemExist != null)
                            {

                             
                                var parcelItemStatusDetail = _unitOfWork.ParcelItemStatusDetail
                                    .GetParcelItemStatusDetail(parcel.ParcelId, item.ParcelItemId,intendedParcelItemStatus.Id);
                                // if status was not set before
                                if (parcelItemStatusDetail == null)
                                {

                                    //  this will update parcel status detail and then call update to parcel current status
                                    UpdateParcelItemStatusDetail(client,appUser, parcelExist, parcelItemExist,intendedParcelItemStatus);
                                    totalitemsUpdated += 1;
                                }

                            }


                        }

                        // check if there is possible next step this parcel can take
                        if (hasNext)
                        {
                            var totalItemsNext = _unitOfWork.ParcelItemStatusDetail.GetParcelItemStatusDetail(parcel.ParcelId, intendedParcelItemStatus.Id).Count();
                            totalitemsUpdated = totalitemsUpdated + totalItemsNext;

                           
                        }
                        else
                        {
                            totalitemsUpdated = _unitOfWork.ParcelItemStatusDetail
                                                            .GetParcelItemStatusDetail(parcel.ParcelId, intendedParcelItemStatus.Id)
                                                              .Count();

                        }

                          if(totalitemsUpdated == totalParcelItems)
                          {
                                UpdateParcelStatusDetail(client, appUser, parcelExist, intendedParcelStatus);
                          }
                          else if(totalitemsUpdated < totalParcelItems)
                          {
                                UpdateParcelStatusDetail(client, appUser, parcelExist, fallbackParcelStatus);
                          }
                        

                     
                    }

                }
            }
        }

        private void UpdateParcelStatusDetail(Client client, AppUser appUser, Parcel parcel, ParcelStatus parcelStatus)
        {
            try
            {
                if (! _unitOfWork.ParcelStatusDetail
                     .CheckParcelHasStatus(client.Id, parcel.Id, parcelStatus.Id))
                {
                    var parcelStatusDetail = new ParcelStatusDetail
                    {
                        AppUser = appUser,
                        Client = client,
                        Parcel = parcel,
                        ParcelStatus = parcelStatus,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };
                    _unitOfWork.ParcelStatusDetail.Add(parcelStatusDetail);
                    _unitOfWork.Complete();

                    UpdateParcelCurrentStatus(client, appUser ,parcel, parcelStatus);
                }
            }
            catch (Exception e)
            {

                throw;
            }

        }

        private void UpdateParcelCurrentStatus(Client client,AppUser appUser, Parcel parcel, ParcelStatus parcelStatus)
        {
            try
            {
                var parcelCurrentStatus = _unitOfWork.ParcelCurrentStatus.GetCurentStatus(client.Id, parcel.Id);

                if (parcelCurrentStatus == null)
                {
                    parcelCurrentStatus = new ParcelCurrentStatus
                    {
                        Client = client,
                        AppUser = appUser,
                        Parcel = parcel,
                        ParcelStatus = parcelStatus,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };

                    _unitOfWork.ParcelCurrentStatus.Add(parcelCurrentStatus);
                    _unitOfWork.Complete();
                }
                else
                {
                    parcelCurrentStatus.Client = client;
                    parcelCurrentStatus.Parcel = parcel;
                    parcelCurrentStatus.ParcelStatus = parcelStatus;
                    parcelCurrentStatus.UpdatedAt = DateTime.Now;

                    _unitOfWork.ParcelCurrentStatus.Update(parcelCurrentStatus);
                    _unitOfWork.Complete();


                }

            }
            catch (Exception e)
            {

                throw;
            }
        }

        private void UpdateParcelItemStatusDetail(Client client,AppUser appUser, Parcel parcel, ParcelItem parcelItem,
                                                        ParcelItemStatus parcelItemStatus)
        {
            try
            {
                var itemStatusDetail = new ParcelItemStatusDetail
                {
                    AppUser = appUser,
                    ParcelItem = parcelItem,
                    Parcel = parcel,
                    ParcelItemStatus = parcelItemStatus,
                    Client = client,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now

                };

                _unitOfWork.ParcelItemStatusDetail.Add(itemStatusDetail);
                _unitOfWork.Complete();

                UpdateParcelItemCurrentStatus(client, appUser, parcel, parcelItem, parcelItemStatus);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private void UpdateParcelItemCurrentStatus(Client client, AppUser appUser, Parcel parcel, ParcelItem parcelItem, ParcelItemStatus parcelItemStatus)
        {
            try
            {
                var itemCurrentStatus = _unitOfWork.ParcelItemCurrentStatus.GetCurrentStatus(parcel.Id, parcelItem.Id);
                if (itemCurrentStatus == null)
                {
                    itemCurrentStatus = new ParcelItemCurrentStatus
                    {
                        Parcel = parcel,
                        AppUser = appUser,
                        ParcelItem = parcelItem,
                        ParcelItemStatus = parcelItemStatus,
                        Client = client,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };

                    _unitOfWork.ParcelItemCurrentStatus.Add(itemCurrentStatus);
                    _unitOfWork.Complete();

                }
                else
                {
                    itemCurrentStatus.ParcelItem = parcelItem;
                    itemCurrentStatus.Parcel = parcel;
                    itemCurrentStatus.AppUser = appUser;
                    itemCurrentStatus.ParcelItemStatus = parcelItemStatus;
                    itemCurrentStatus.Client = client;
                    itemCurrentStatus.UpdatedAt = DateTime.Now;

                    _unitOfWork.ParcelItemCurrentStatus.Update(itemCurrentStatus);
                    _unitOfWork.Complete();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.AppUser
{
    public class AppUserViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Username")]
        public string Username { get; set; }
        [Display(Name = "Full Name")]
        public string Fullname { get; set; }
        [Display(Name = "Phone number")]
        public string Phone { get; set; }
        [Display(Name = "Active")]
        public bool Active { get; set; }
        [Display(Name ="Branch")]
        public string BranchName { get; set; }
        [Display(Name ="Created on")]
        public DateTime CreatedAt { get; set; }
        [Display(Name ="Last Modified")]
        public DateTime UpdatedAt { get; set; }
    }

    public class AppUserIndexViewModel
    {
        public AppUserViewModel AppUserViewModel { get; set; }
        public IEnumerable<AppUserViewModel> ListAppUserViewModel { get; set; }
      
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.AppUser
{
    public class AppUserInfoViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Phone { get; set; }
        public bool Active { get; set; }
        public string BranchName { get; set; }
        public int BranchId { get; set; }
        public string ClientName { get; set; }
        public int ClientId { get; set; }
        public int OriginId { get; set; }
        public string OriginName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}

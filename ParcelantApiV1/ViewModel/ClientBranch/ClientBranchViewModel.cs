﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.ClientBranch
{
    public class ClientBranchViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Branch Name")]
        public string Name { get; set; }
        [Display(Name ="County")]
        public String CountyName { get; set; }
        
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Phone Numer")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name ="Active")]
        public bool Active { get; set; }

        [Display(Name ="Created On")]
        public DateTime CreatedAt { get; set; }

        [Display(Name ="Last Modified")]
        public DateTime UpdatedAt { get; set; }
    }
}

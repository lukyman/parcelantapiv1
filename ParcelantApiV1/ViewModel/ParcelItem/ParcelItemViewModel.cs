﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.ParcelItem
{
    public class ParcelItemViewModel
    {
        public int Id { get; set; }
        public int ParcelId { get; set; }
        public int AppUserId { get; set; }
        public string AppUserName { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class ParcelItemStatusDetailViewModel:ParcelItemViewModel{
        public int ItemStatusId { get; set; }
        public string ItemStatusName { get; set; }
    }

    public class ParcelItemCurrentStatusViewModel : ParcelItemStatusDetailViewModel
    {

    }
}

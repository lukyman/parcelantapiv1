﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.RouteStop
{
    public class RouteStopViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Location")]
        public string Name { get; set; }
        [Display(Name="Created on")]
        public DateTime CreatedAt { get; set; }
        [Display(Name ="Last modified")]
        public DateTime UpdatedAt { get; set; }

    }
}

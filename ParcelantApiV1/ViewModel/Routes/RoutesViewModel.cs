﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.Routes
{
    public class RoutesViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Origin")]
        public string Origin { get; set; }
        [Display(Name = "Destination")]
        public string Destination { get; set; }
        [Display(Name ="Created on")]
        public DateTime CreatedAt { get; set; }
        [Display(Name ="Last modified")]
        public DateTime UpdatedAt { get; set; }
    }
}

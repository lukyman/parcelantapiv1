﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.UpdateParcelStatus
{
    public class UpdateParcelStatusViewModel
    {
        public int AppUserId { get; set; }
        public IEnumerable<UpdateParcelStatus> UpdateParcelStatus { get; set; }
    }

    public class UpdateParcelStatus
    {
        public int ParcelId { get; set; }

        public IEnumerable<UpdateParcelItemStatus> ParcelItems { get; set; }
    }

   public class UpdateParcelItemStatus
   {
        public int ParcelItemId { get; set; }
      }
}

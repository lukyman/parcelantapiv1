﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelantApiV1.ViewModel.ParcelStatus
{
    public class ParcelStatusDetailViewModel
    {
        public string ParcelNumber { get; set; }
        public string Description { get; set; }
        public string StatusName { get; set; }
        public string StatusDescription { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }


}

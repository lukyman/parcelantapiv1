﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.Parcel;
using ParcelantApiV1.ViewModel.ParcelItem;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/Receive")]
    public class ReceiveController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private IParcelStatusManager _parcelStatusManager; 
        public ReceiveController(IUnitOfWork unitOfWork, IMapper mapper, IParcelStatusManager parcelStatusManager)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _parcelStatusManager = parcelStatusManager;

        }
        // GET: api/Receive/Parcels-To-Receive/5/4"
        [HttpGet("Parcels-To-Receive/{clientId}/{branchId}")]
        [Authorize]
        public IActionResult GetParcelsToReceive(int clientId, int branchId)
        {
            var response = new ListDataResponse<ParcelToReceiveViewModel>();
            if (_unitOfWork.Clients.Get(clientId) == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            if (_unitOfWork.ClientBranch.GetClientBranch(clientId, branchId) == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }


            try
            {
                var destination = _unitOfWork.ClientBranchRouteStop.GetBranchDestination(clientId, branchId);
                var intendedParcelStatus = _unitOfWork.ParcelStatus.GetParcelStatusByName(ParcelStatusTrack.DISPATCHED_FROM_THE_SOURCE);
                var fallbackParcelStatus = _unitOfWork.ParcelStatus.GetParcelStatusByName(ParcelStatusTrack.DISPATCHED_SOME_ITEMS);

                /*var parcelsToReceive = _unitOfWork.ParcelCurrentStatus.GetAllParcelCurrentStatus(
                    clientId, 
                    null, // branchid
                    intendedParcelStatus.Id, 
                    fallbackParcelStatus.Id,
                    destination.RouteStop.Id);
                    */
             var   parcelsToReceive = _unitOfWork.ParcelCurrentStatus.GetAllParcelsToReceive(
                        clientId,
                        branchId,
                        intendedParcelStatus.Id,
                        fallbackParcelStatus.Id,
                        destination.RouteStop.Id);

                var parcelToReceive = _mapper.Map<IEnumerable<ParcelCurrentStatus>, IEnumerable<ParcelToReceiveViewModel>>(parcelsToReceive);

                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = parcelToReceive;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = " Unable to get parcels";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.ListResponse();

        }

        // GET: api/Receive/Parcelitems-to-Receive/5/4/3
        [HttpGet("Parcelitems-to-Receive/{clientId}/{branchId}/{parcelId}")]
        [Authorize]
        public IActionResult GetParcelItemsToReceive(int clientId, int branchId, int parcelId)
        {
            var response = new ListDataResponse<ParcelItemViewModel>();
            if (_unitOfWork.Clients.Get(clientId) == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            if (_unitOfWork.ClientBranch.GetClientBranch(clientId, branchId) == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            try
            {
                var parcelStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.ITEM_DISPATCHED_FROM_SOURCE);

                var parcelItemsToReceive = _mapper.Map<IEnumerable<ParcelItemCurrentStatus>, IEnumerable<ParcelItemCurrentStatusViewModel>>(
                _unitOfWork.ParcelItemCurrentStatus.GetParcelItemsCurrentStatus(clientId, parcelId, parcelStatus.Id));

                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = parcelItemsToReceive;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to get parcel items";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.ListResponse();
        }
        
        // POST: api/Receive
        [HttpPost("{clientId}/{branchId}")]
        [Authorize]
        public IActionResult Post([FromBody] UpdateParcelStatusViewModel updateParcelStatusViewModel, int clientId, int branchId)
        {
            var response = new SingleDataResponse<bool>();

            var client = _unitOfWork.Clients.Get(clientId);
            if (client == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();
            }

            var clientBranch = _unitOfWork.ClientBranch.Get(branchId);
            if(clientBranch == null)
            {

                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();
            }
            var clientAppUser = _unitOfWork.ClientAppUser.GetAppUserInfo(updateParcelStatusViewModel.AppUserId);
            if (clientAppUser.Client.Id != clientId && clientAppUser.ClientBranch.Id != branchId)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();

            }
            var intendendParcelStatus = _unitOfWork.ParcelStatus
                               .GetParcelStatusByName(ParcelStatusTrack.RECEIVED_ALL_ITEMS_AT_DESTINATION);
            var fallbackParcelStatus = _unitOfWork.ParcelStatus
                               .GetParcelStatusByName(ParcelStatusTrack.RECEIVED_SOME_ITEMS_AT_DESTINATION);


            var appUser = clientAppUser.AppUser;

            var parcelItemStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.ITEM_RECEIVED_AT_DESTINATION);
            var fromParcelItemStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.ITEM_DISPATCHED_FROM_SOURCE);

            // index that their is next posible status to be added or already added
            var hasNext = true;


            try
            {
                _parcelStatusManager.UpdateParcelStatus(client, clientBranch, appUser, updateParcelStatusViewModel,
                                                       intendendParcelStatus, fallbackParcelStatus, parcelItemStatus,fromParcelItemStatus,hasNext);

                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = true;

            }
            catch (Exception e)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unable to update parcel status";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.SingleResponse();
        }
        
    }
}

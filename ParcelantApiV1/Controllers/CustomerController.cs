﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using System;
using System.Collections.Generic;
using System.Net;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private IUnitOfWork _unitOfWork;
        
        public CustomerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        

        [HttpGet]
        [Authorize]
        public IActionResult GetCustomerByPhone(string phone)
        {
            var response = new SingleDataResponse<Customer>();
            try
            {
                phone = Util.PhoneNumberFormatter.formatPhoneNumber(Convert.ToInt64(phone));
                var data = _unitOfWork.Customer.GetCustomerByPhone(phone);

                response.Error = false;
                response.Data = data;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Cannot find the customer account";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.SingleResponse();
        }

        // GET: api/Customer/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Customer
        [HttpPost]
        [Authorize]
        public IActionResult Post(Customer customer)
        {
            var response = new SingleDataResponse<Customer>();
            customer.Phone = Util.PhoneNumberFormatter.formatPhoneNumber(Convert.ToInt64(customer.Phone));
            var customerExist = _unitOfWork.Customer.GetCustomerByPhone(customer.Phone);
            if (customerExist!=null)
            {
                response.Error = false;
                response.Data = customer;
                response.Status = HttpStatusCode.Created;
                return response.SingleResponse();
            }
            try
            {
                customer.CreatedAt = DateTime.Now;
                customer.UpdatedAt = DateTime.Now;

                _unitOfWork.Customer.Add(customer);
                _unitOfWork.Complete();

                response.Error = false;
                response.Data = customer;
                response.Status = HttpStatusCode.Created;
            }
            catch (System.Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to create the customer account";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.SingleResponse();
        }
        
        // PUT: api/Customer/5
        [HttpPost("{customerid}")]
        [Authorize]
        public IActionResult Put(int customerid, Customer customer)
        {
            var response = new SingleDataResponse<Customer>();
            
            customer.Phone = Util.PhoneNumberFormatter.formatPhoneNumber(Convert.ToInt64(customer.Phone));
            var _customer = _unitOfWork.Customer.GetCustomerByPhone(customer.Phone);
            if (_customer.Id != customerid)
            {
                response.Error = true;
                response.ErrorMessage ="The phone number"+customer.Phone+ " belongs to another account";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();
            }
            try
            {
                _customer.Phone = customer.Phone;
                _customer.FullName = customer.FullName;
                _customer.UpdatedAt = DateTime.Now;
                _unitOfWork.Customer.Update(_customer);
                _unitOfWork.Complete();

                response.Error = false;
                response.Data = _customer;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Cannot find the customer account";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.SingleResponse();
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

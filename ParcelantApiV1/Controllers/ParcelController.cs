﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AspNet.Security.OAuth.Validation;
using AspNet.Security.OpenIdConnect.Server;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.Parcel;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/Parcel")]
    
    public class ParcelController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public ParcelController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        // GET: api/Parcel
        [HttpGet("{clientId}")]
        [Authorize]
        public IActionResult Get(int clientId)
        {
            var response = new ListDataResponse<ParcelViewModel>();
            
            try
            {
                var parcelDetail = _unitOfWork.Parcel.GetParcels(clientId);
                var data = _mapper.Map<IEnumerable<Parcel>,IEnumerable<ParcelViewModel>>(parcelDetail);

                response.Data = data;
                response.Error = false;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to get the parcel detail";
                response.Status = HttpStatusCode.BadRequest;
            }
            
            return  response.ListResponse();
           
        }

        // GET: api/Parcel/5
        [HttpGet("{clientId}/{parcelId}")]
        [Authorize]
        public IActionResult Get(int clientId,int parcelId)
        {
            var response = new SingleDataResponse<ParcelViewModel>();
            try
            {
                var parcelDetail = _unitOfWork.Parcel.GetParcelDetail(clientId, parcelId);
                var data = _mapper.Map<Parcel, ParcelViewModel>(parcelDetail);

                response.Data = data;
                response.Error = false;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to get the parcel detail";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.SingleResponse();
        }
        
        // POST: api/Parcel
        [HttpPost("client/{_clientid}/branch/{_branchid}")]
        [Authorize]
        public IActionResult Post(CreateParcelViewModel parcelViewModel, int _clientid, int _branchid)
        {
            var response = new SingleDataResponse<ParcelViewModel>();
            try
            {
                var origin = _unitOfWork.ClientBranchRouteStop.GetBranchOrigin(_clientid, _branchid);
                if (origin.Id != parcelViewModel.DestinationId)
                {

                }

                var route = _unitOfWork.Route.GetRouteByOrginAndDestination(_clientid,  origin.Id, parcelViewModel.DestinationId);
                var parcelNumerCounter = _unitOfWork.ClientParcelNumberCounter
                                            .GetClientCurrentCounter(parcelViewModel.ClientId);

                if (parcelNumerCounter== null)
                {
                    var counter = new ClientParcelNumberCounter
                    {
                        Client = _unitOfWork.Clients.Get(parcelViewModel.ClientId),
                        ParcelNumber = 1,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };

                    _unitOfWork.ClientParcelNumberCounter.Add(counter);
                    _unitOfWork.Complete();

                    parcelNumerCounter = counter;
                }
                else
                {
                    parcelNumerCounter.ParcelNumber =MakeParcelNumber.Make(parcelNumerCounter.ParcelNumber);
                    parcelNumerCounter.UpdatedAt = DateTime.Now;
                    _unitOfWork.ClientParcelNumberCounter.Update(parcelNumerCounter);
                    _unitOfWork.Complete();
                }

                var parcelNumber = FormatParcelNumber.Format(parcelNumerCounter.ParcelNumber);
                var appuser = _unitOfWork.AppUser.Get(parcelViewModel.AppUserId);
                var client = _unitOfWork.Clients.Get(parcelViewModel.ClientId);

                var parcel = new Parcel
                {
                    AppUser = appuser,
                    Client = client,
                    ClientBranch = _unitOfWork.ClientBranch.Get(parcelViewModel.ClientBranchId),
                    Receiver = _unitOfWork.Customer.Get(parcelViewModel.ReceiverId),
                    Sender = _unitOfWork.Customer.Get(parcelViewModel.SenderId),
                    CollectionPoint = _unitOfWork.CollectionPoint.Get(parcelViewModel.CollectionPointId),
                    Routes = route,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    ParcelNumber = parcelNumber
                };

                _unitOfWork.Parcel.Add(parcel);
                _unitOfWork.Complete();

                var parcelStatus = _unitOfWork.ParcelStatus.GetParcelStatusByName(ParcelStatusTrack.RECORDED);

                # region setup parcel status not created before hand by parcelant admin
                if (parcelStatus == null) {
                    parcelStatus = new ParcelStatus
                    {
                        Name = ParcelStatusTrack.RECORDED,
                        Description = "Recorded at the source",
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };

                    _unitOfWork.ParcelStatus.Add(parcelStatus);
                    _unitOfWork.Complete();
                }

                #endregion

                #region Setup parcel status detail and save 

                var parcelStatusDetail = new ParcelStatusDetail
                {
                    AppUser = appuser,
                    Client = client,
                    Parcel = parcel,
                    ParcelStatus = parcelStatus,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };

                _unitOfWork.ParcelStatusDetail.Add(parcelStatusDetail);
                _unitOfWork.Complete();
                #endregion

                #region update parcel current status
                var parcelCurrentStatus = _unitOfWork.ParcelCurrentStatus.GetCurentStatus(client.Id,parcel.Id);

                // create parcel current status
                if (parcelCurrentStatus == null)
                {
                    var createParcelCurrentStatus = new ParcelCurrentStatus
                    {
                        Client = client,
                        AppUser = appuser,
                        Parcel = parcel,
                        ParcelStatus = parcelStatus,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };

                    _unitOfWork.ParcelCurrentStatus.Add(createParcelCurrentStatus);
                    _unitOfWork.Complete();

                }
                #endregion

                var parcelDetail =  _unitOfWork.Parcel.GetParcelDetail(parcel.Client.Id, parcel.Id);

                response.Data = _mapper.Map<Parcel, ParcelViewModel>(parcelDetail);
                response.Error = false;
                response.Status = HttpStatusCode.Created;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to create Parcel";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.SingleResponse();
        }
        
       
    }
}

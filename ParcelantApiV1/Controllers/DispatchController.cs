﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.Parcel;
using ParcelantApiV1.ViewModel.ParcelItem;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/Dispatch")]
    public class DispatchController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IParcelStatusManager _parcelStatusManager;
        private IDispatchManager _dispatchManager;
        private IMapper _mapper;
        public DispatchController(IUnitOfWork unitOfWork, 
            IParcelStatusManager parcelStatusManager,
            IDispatchManager dispatchManager,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _parcelStatusManager = parcelStatusManager;
            _dispatchManager = dispatchManager;
            _mapper = mapper;
        }
        // GET: api/Dispatch/4
        [HttpGet("Parcelstodispatch/{clientid}/{branchid}")]
        [Authorize]
        public IActionResult GetParcelsToDispatch(int clientId, int branchId)
        {
            var response = new ListDataResponse<ParcelToDispatchViewModel>();
            if (_unitOfWork.Clients.Get(clientId) == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            if (_unitOfWork.ClientBranch.GetClientBranch(clientId, branchId)==null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            try
            {
                var intendedParcelStatus = _unitOfWork.ParcelStatus.GetParcelStatusByName(ParcelStatusTrack.RECORDED);
                var fallbackParcelStatus = _unitOfWork.ParcelStatus.GetParcelStatusByName(ParcelStatusTrack.DISPATCHED_SOME_ITEMS);

                var parcelStatus = _unitOfWork.ParcelCurrentStatus.GetAllParcelCurrentStatus(clientId, 
                                                                                              branchId, 
                                                                                              intendedParcelStatus.Id, 
                                                                                              fallbackParcelStatus.Id);
                var parcelToDispatch = _mapper.Map<IEnumerable<ParcelCurrentStatus>,IEnumerable<ParcelToDispatchViewModel>>(parcelStatus);
                
                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = parcelToDispatch;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = " Unable to get parcels";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.ListResponse();

        }

        // GET: api/Dispatch/5/4/3
        [HttpGet("parcelitemstodispatch/{clientId}/{branchId}/{parcelId}")]
        [Authorize]
        public IActionResult GetParcelItemsToDispatch(int clientId, int branchId, int parcelId)
        {
            var response = new ListDataResponse<ParcelItemViewModel>();
            if (_unitOfWork.Clients.Get(clientId) == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            if (_unitOfWork.ClientBranch.GetClientBranch(clientId, branchId) == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            try
            {
                var parcelStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.RECORDED);

                var parcelItemsToDispatch = _mapper.Map<IEnumerable<ParcelItemCurrentStatus>,IEnumerable<ParcelItemCurrentStatusViewModel>>(
                _unitOfWork.ParcelItemCurrentStatus.GetParcelItemsCurrentStatus(clientId,parcelId,parcelStatus.Id));

                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = parcelItemsToDispatch;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to get parcel items";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.ListResponse();
        }
        
        // POST: api/Dispatch
        [HttpPost("{clientId}/{branchId}")]
        [Authorize]
        public IActionResult Post([FromBody]UpdateParcelStatusViewModel updateParcelStatusViewModel, int clientId, int branchId, int transitAppUserId, int carrierid)
        {
            var response = new SingleDataResponse<bool>();
            if (updateParcelStatusViewModel == null)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "No data passed";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();
            }

            var client = _unitOfWork.Clients.Get(clientId);
            if (client == null)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();

            }

            var clientBranch = _unitOfWork.ClientBranch.GetClientBranch(clientId, branchId);
            if (clientBranch == null)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();

            }


            var transitAppUser = _unitOfWork.AppUserRole.GetAppUserRoleByCode(clientId, transitAppUserId, "202");
            if (transitAppUser == null)
            {
                response.Error = true;
                response.ErrorMessage = "to dispatch, you have to appoint a user to deliver the parcels eg. driver";
                response.Status = HttpStatusCode.BadRequest;
                response.Data = false;
                return response.SingleResponse();
            }

            var carrier = _unitOfWork.Carrier.Get(carrierid);
            if (carrier == null)
            {
                response.Error = true;
                response.ErrorMessage = "to dispatch, you have to appoints a carrier to deliver the parcels eg. Bus, Lorry";
                response.Status = HttpStatusCode.BadRequest;
                response.Data = false;
                return response.SingleResponse();
            }

            var clientAppUser = _unitOfWork.ClientAppUser.GetAppUserInfo(updateParcelStatusViewModel.AppUserId);
            if (clientAppUser.Client.Id != clientId && clientAppUser.ClientBranch.Id!=branchId)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();

            }

        try
            {

            var intendendParcelStatus =  _unitOfWork.ParcelStatus
                               .GetParcelStatusByName(ParcelStatusTrack.DISPATCHED_FROM_THE_SOURCE);
            var fallbackParcelStatus =  _unitOfWork.ParcelStatus
                               .GetParcelStatusByName(ParcelStatusTrack.DISPATCHED_SOME_ITEMS);

            var fromParcelItemStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.RECORDED);

            var appUser = clientAppUser.AppUser;

            var intendedParcelItemStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.ITEM_DISPATCHED_FROM_SOURCE);

            var hasNext = true;

            
                _parcelStatusManager.UpdateParcelStatus(client,clientBranch,appUser,updateParcelStatusViewModel,
                                                        intendendParcelStatus,fallbackParcelStatus,intendedParcelItemStatus,fromParcelItemStatus,hasNext);

                 var parcelsToDispatch = updateParcelStatusViewModel.UpdateParcelStatus.Select(x => x.ParcelId).ToList();

                _dispatchManager.ProcessClientDispatch(client, clientBranch, appUser, carrier, parcelsToDispatch);
               

                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = true;
            
            }
            catch (Exception e)
            {
                response.Data = false;
                response.Error = true;
                response.ErrorMessage = "Unable to update parcel status";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.SingleResponse();
        }
        
        [HttpGet("parcel/{clientid}/{branchid}")]
        [Authorize]
        public IActionResult GetDispatchedParcels(int clientid,int branchid)
        {
            var response = new ListDataResponse<ParcelToDispatchViewModel>();

            var clientBranch = _unitOfWork.ClientBranch.GetClientBranch(clientid, branchid);
            if (clientBranch == null)
            {
                response.Error = true;
                response.ErrorMessage = "Unauthorized user";
                response.Status = HttpStatusCode.BadGateway;

                return response.ListResponse();
            }

            try
            {
                var intendedStatus = _unitOfWork.ParcelStatus.GetParcelStatusByName(ParcelStatusTrack.DISPATCHED_FROM_THE_SOURCE);
                var fallbackStatus = _unitOfWork.ParcelStatus.GetParcelStatusByName(ParcelStatusTrack.DISPATCHED_SOME_ITEMS);
                var parcel = _unitOfWork.ParcelCurrentStatus.GetAllParcelCurrentStatus(clientid,
                                                                                        branchid,
                                                                                        intendedStatus.Id,
                                                                                        fallbackStatus.Id);

                var dispatchedParcel = _mapper.Map<IEnumerable<ParcelCurrentStatus>, IEnumerable<ParcelToDispatchViewModel>>(parcel);

                response.Error = false;
                response.Status = HttpStatusCode.OK;
                response.Data = dispatchedParcel;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to get parcels";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.ListResponse();
        }
    }
}

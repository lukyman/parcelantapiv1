﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.Routes;
using ParcelantApiV1.ViewModel.RouteStop;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/Route")]
    public class RouteController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public RouteController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        [Authorize]
        [HttpGet("{clientId}")]
        public IActionResult Get(int clientId)
        {
            var response = new ListDataResponse<RoutesViewModel>();
            try
            {
                var  routes =  _unitOfWork.Route.GetClientRoutes(clientId);
                var routeViewModel = _mapper.Map<IEnumerable<Routes>,IEnumerable<RoutesViewModel>>(routes);

                response.Error = false;
                response.Data = routeViewModel;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.Status = HttpStatusCode.BadRequest;
                response.ErrorMessage = "Unable to fetch routes";
            }

            return response.ListResponse();
        }

        [Authorize]
        [HttpGet("destination/client/{clientId}/branch/{branchid}")]
        public IActionResult GetBranchRoutes(int clientId, int branchid)
        {
            var response = new ListDataResponse<RouteStopViewModel>();



            try
            {
                var routes = _unitOfWork.Route.GetClientBranchRoutes(clientId, branchid);
                var routeViewModel = _mapper.Map<IEnumerable<RouteStop>, IEnumerable<RouteStopViewModel>>(routes);

                response.Error = false;
                response.Data = routeViewModel;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.Status = HttpStatusCode.BadRequest;
                response.ErrorMessage = "Unable to fetch routes";
            }

            return response.ListResponse();
        }

        [Authorize]
        [HttpGet("{clientId}/{routeId}")]
        public IActionResult Get(int clientId, int routeId)
        {
            var response = new SingleDataResponse<RoutesViewModel>();
            try
            {
                var routes = _unitOfWork.Route.GetClientRoute(clientId,routeId);
                var routeViewModel = _mapper.Map<Routes, RoutesViewModel>(routes);

                response.Error = false;
                response.Data = routeViewModel;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.Status = HttpStatusCode.BadRequest;
                response.ErrorMessage = "Unable to fetch the route";
            }
            return response.SingleResponse();
        }

    }
}
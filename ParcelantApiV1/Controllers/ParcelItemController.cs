﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.BusinessLogic.Domain;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.ParcelItem;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/ParcelItem")]
    public class ParcelItemController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public ParcelItemController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        // GET: api/ParcelItem
        [HttpGet("{clientid}/{parcelid}")]
        [Authorize]
        public IActionResult Get(int clientid, int parcelid)
        {
            var response = new ListDataResponse<ParcelItemViewModel>();
            try
            {
                var parcelitems = _unitOfWork.ParcelItem.GetParcelItems(parcelid);
                var data = _mapper.Map<IEnumerable<ParcelItem>, IEnumerable<ParcelItemViewModel>>(parcelitems);

                response.Data = data;
                response.Error = false;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Status = HttpStatusCode.BadRequest;
                response.ErrorMessage = "Unable to fetch data";
                response.Error = true;
            }
            return response.ListResponse();
        }

        // GET: api/ParcelItem/5
        [HttpGet("{clientid}/{parcelid}/{parcelitemid}")]
        [Authorize]
        public IActionResult Get(int clientid, int parcelid, int parcelitemid)
        {
            var response = new SingleDataResponse<ParcelItemViewModel>();
            try
            {
                var parcelitem = _unitOfWork.ParcelItem.GetParcelItem(parcelid, parcelitemid);
                var data = _mapper.Map<ParcelItem, ParcelItemViewModel>(parcelitem);

                response.Data = data;
                response.Error = false;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Status = HttpStatusCode.BadRequest;
                response.ErrorMessage = "Unable to fetch data";
                response.Error = true;
            }
            return response.SingleResponse();
        }

        // POST: api/ParcelItem
        [HttpPost("{_clientid}/{_parcelid}")]
        [Authorize]
        public IActionResult Post(int _clientid, int _parcelid, CreateParcelItemViewModel parcelItem)
        {
            var response = new SingleDataResponse<ParcelItemViewModel>();

            if (_clientid != parcelItem.ClientId && _parcelid != parcelItem.ParcelId)
            {
                response.ErrorMessage = "Not Authorized";
                response.Error = true;
                response.Status = HttpStatusCode.BadRequest;
                return response.SingleResponse();
            }

            try
            {
                var appUser = _unitOfWork.AppUser.Get(parcelItem.AppUserId);
                var parcel = _unitOfWork.Parcel.Get(parcelItem.ParcelId);
                var client = _unitOfWork.Clients.Get(_clientid);

                var createParcelItem = new ParcelItem {
                    Amount = parcelItem.Amount,
                    AppUser = appUser,
                    Parcel = parcel,
                    Quantity = parcelItem.Quantity,
                    Description = parcelItem.Description,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };
                
                _unitOfWork.ParcelItem.Add(createParcelItem);
                _unitOfWork.Complete();


                // parcel item status 
                var parcelItemStatus = _unitOfWork.ParcelItemStatus.GetParcelItemStatusByName(ParcelStatusTrack.RECORDED);

                // add parcel item status detail
                var parcelItemStatusDetail = new ParcelItemStatusDetail
                {
                    AppUser = appUser,
                    Parcel = parcel,
                    Client = client,
                    ParcelItem = createParcelItem,
                    ParcelItemStatus = parcelItemStatus,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };

                _unitOfWork.ParcelItemStatusDetail.Add(parcelItemStatusDetail);
                _unitOfWork.Complete();


                // update parcel item current status

                var parcelItemCurrentStatus = _unitOfWork.ParcelItemCurrentStatus
                            .GetCurrentStatus(parcel.Id,createParcelItem.Id);
                if (parcelItemCurrentStatus==null)
                {
                    var createParcelItemCurrentStatus = new ParcelItemCurrentStatus
                    {
                        Client = client,
                        Parcel = parcel,
                        AppUser = appUser,
                        ParcelItem = createParcelItem,
                        ParcelItemStatus = parcelItemStatus,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };

                    _unitOfWork.ParcelItemCurrentStatus.Add(createParcelItemCurrentStatus);
                    _unitOfWork.Complete();
                }

                // after creating parcel item get the created item
                var getParcelItem = _unitOfWork.ParcelItem.GetParcelItem(_parcelid, createParcelItem.Id);
                response.Data =  _mapper.Map<ParcelItem, ParcelItemViewModel>(getParcelItem);

                response.Error = false;
                response.Status = HttpStatusCode.Created;

            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to add item";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.SingleResponse();
        }
        
        // PUT: api/ParcelItem/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

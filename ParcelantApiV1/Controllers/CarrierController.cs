﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.Carrier;

namespace ParcelantApiV1.Controllers
{

  [Produces("application/json")]
  [Route("api/Carrier")]
  public class CarrierController : Controller
  {
    private IUnitOfWork _unitOfWork;
    private IMapper _mapper;
    public CarrierController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    // GET: api/Carrier/8
    [HttpGet("{clientid}")]
    [Authorize]
    public IActionResult GetAllClientCarriers(int clientid)
    {
      var response = new ListDataResponse<CarrierViewModel>();
      var client = _unitOfWork.Clients.Get(clientid);
      if (client == null)
      {
        response.Error = true;
        response.ErrorMessage = "Unauthorized user";
        response.Status = HttpStatusCode.BadRequest;
        return response.ListResponse();
      }
      try
      {
        var clientCarriers = _unitOfWork.Carrier.GetClientCarriers(clientid);
        var carriers = _mapper.Map<IEnumerable<Carrier>, IEnumerable<CarrierViewModel>>(clientCarriers);

        response.Error = false;
        response.Data = carriers;
        response.Status = HttpStatusCode.OK;
      }
      catch (Exception e)
      {
        response.Error = true;
        response.ErrorMessage = "Unable to get client carriers";
        response.Status = HttpStatusCode.BadRequest;

      }
      return response.ListResponse();
    }
    
  }
}

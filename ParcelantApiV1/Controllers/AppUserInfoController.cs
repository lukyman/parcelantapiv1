﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Server;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;
using ParcelantApiV1.ViewModel.AppUser;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/AppUserInfo")]
    public class AppUserInfoController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public AppUserInfoController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetAppUserInfo()
        {
            var response = new SingleDataResponse<AppUserInfoViewModel>();
            try
            {
                var claimIdentity = HttpContext.User.Claims as ClaimsIdentity;
                var username = User.Claims.Where(x=>x.Type=="name").FirstOrDefault().Value;
               // var info = await HttpContext.AuthenticateAsync(OpenIdConnectServerDefaults.AuthenticationScheme);
                var appUser = _unitOfWork.AppUser.GetAppUserByUsername(username);
                var appUserInfo = _unitOfWork.ClientAppUser.GetAppUserInfo(appUser.Id);
                var branchRouteStop = _unitOfWork.ClientBranchRouteStop.GetBranchOrigin(appUserInfo.Client.Id, appUserInfo.ClientBranch.Id);

                var appUserInfoViewModel = _mapper.Map<ClientAppUser, AppUserInfoViewModel>(appUserInfo);
                if (branchRouteStop != null)
                {
                    appUserInfoViewModel.OriginId = branchRouteStop.RouteStop.Id;
                    appUserInfoViewModel.OriginName = branchRouteStop.RouteStop.Name;
                }
                response.Data = appUserInfoViewModel;
                response.Error = false;
                response.Status = HttpStatusCode.OK;


            }
            catch (Exception e)
            {
                response.Error = false;
                response.ErrorMessage = "Unable to get App User info";
                response.Status = HttpStatusCode.BadRequest;
            }

            return response.SingleResponse();
         
        }
    }
}
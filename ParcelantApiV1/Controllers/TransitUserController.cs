﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.Util.RestResponse;

namespace ParcelantApiV1.Controllers
{
  [Produces("application/json")]
  [Route("api/TransitUser")]
  public class TransitUserController : Controller
  {
    private IUnitOfWork _unitOfWork;
    private IMapper _mapper;
    private string roleCode = "202";
    public TransitUserController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }
    // GET: api/TransitUser
    [HttpGet("{clientid}")]
    [Authorize]
    public IActionResult Get(int clientid)
    {
      var response = new ListDataResponse<AppUserRole>();
      var client = _unitOfWork.Clients.Get(clientid);
      if (client == null)
      {
        response.Error = true;
        response.ErrorMessage = "Unauthorized user";
        response.Status = System.Net.HttpStatusCode.BadRequest;
        return response.ListResponse();
      }

      try
      {
        var transitAppUser = _unitOfWork.AppUserRole.GetAppUsersRoleByCode(clientid,roleCode);
        response.Error = false;
        response.Status = HttpStatusCode.OK;
        response.Data = transitAppUser;
      }
      catch (Exception e) 
      {

        throw;
      }
      return response.ListResponse();
      
    }

  }
}

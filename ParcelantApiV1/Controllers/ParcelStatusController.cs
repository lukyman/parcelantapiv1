﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.DAL.Models;
using ParcelantApiV1.ViewModel.UpdateParcelStatus;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/ParcelStatus")]
    public class ParcelStatusController : Controller
    {
        private IUnitOfWork _unitOfWork;
        
        public ParcelStatusController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        // POST: api/ParcelStatus
        [HttpPost("{clientid}")]
        public void Post([FromBody] UpdateParcelStatusViewModel updateParcelStatusViewModel, int clientid)
        {
            var client = _unitOfWork.Clients.Get(clientid);
            if (client == null)
            {

            }

            var appUser = _unitOfWork.ClientAppUser.GetAppUserInfo(updateParcelStatusViewModel.AppUserId);
            if(appUser.Client.Id != clientid)
            {

            }

            foreach (var parcel in updateParcelStatusViewModel.UpdateParcelStatus)
            {
                var _parcel = _unitOfWork.Parcel.Get(parcel.ParcelId);

                var totalitems = _unitOfWork.ParcelItem.GetParcelItems(_parcel.Id).Count();
                int totalitemsUpdated = 0; 
                foreach (var item in parcel.ParcelItems)
                {
                   // var itemstatus = _unitOfWork.ParcelItemStatus.Get(item.ParcelItemStatusId);

                    //get current item status detail
                   // var itemstatusdetail = _unitOfWork.ParcelItemStatusDetail.GetParcelItemStatusDetail(_parcel.Id, item.ParcelItemId, itemstatus.Id);
                   // if (itemstatusdetail == null)
                   // {

                     //   UpdateParcelItemDetail(clientid,_parcel.Id,item.ParcelItemId,item.ParcelItemStatusId);
                  //      totalitemsUpdated += 1;
                 //   }
                    
                }
            }
            

        }

        private void UpdateParcelItemDetail(int clientId, int parcelId, int parcelItemId,int parcelItemStatusId)
        {
            var itemStatusDetail = new ParcelItemStatusDetail
            {
                Client = _unitOfWork.Clients.Get(clientId),

            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParcelantApiV1.Businesslogic;
using ParcelantApiV1.DAL.Model;
using ParcelantApiV1.Util.RestResponse;

namespace ParcelantApiV1.Controllers
{
    [Produces("application/json")]
    [Route("api/CollectionPoint")]
    public class CollectionPointController : Controller
    {
        private IUnitOfWork _unitOfWork;
        public CollectionPointController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Authorize]
        [HttpGet("{clientId}")]
        public IActionResult Get(int clientId)
        {
            var response = new ListDataResponse<CollectionPoint>();
            try
            {
               var collectionPoints = _unitOfWork.CollectionPoint.GetClientCollectionPoints(clientId);
                response.Error = false;
                response.Data = collectionPoints;
                response.Status = HttpStatusCode.OK; 
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to fetch collection points";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.ListResponse();
        }

        [Authorize]
        [HttpGet("{clientId}/{collectionPointId}")]
        public IActionResult Get(int clientId,int  collectionPointId)
        {
            var response = new SingleDataResponse<CollectionPoint>();
            try
            {
                var collectionPoints = _unitOfWork.CollectionPoint.GetClientCollectionPoint(clientId,collectionPointId);
                response.Error = false;
                response.Data = collectionPoints;
                response.Status = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.Error = true;
                response.ErrorMessage = "Unable to fetch collection points";
                response.Status = HttpStatusCode.BadRequest;
            }
            return response.SingleResponse();
        }
    }
}
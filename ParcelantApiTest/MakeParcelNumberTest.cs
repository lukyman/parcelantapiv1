﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParcelantApiV1.BusinessLogic.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParcelantApiTest
{
    [TestClass]
    public class MakeParcelNumberTest
    {

        [TestMethod]
        public void TakesLongNumber_ShouldIncrementByOne()
        {
            var result = MakeParcelNumber.Make(1);

            Assert.AreEqual(result, 2);
        }

    }
}
